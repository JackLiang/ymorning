package com.ymorning.common.main;

import java.util.TimerTask;
import org.apache.log4j.Logger;

import com.ymorning.common.utils.CacheManager;
import com.ymorning.common.utils.GetUrlContext;

public class FlightDataTimerTask extends TimerTask {
	private static Logger log = Logger.getLogger(FlightDataTimerTask.class);
	@Override
	public void run() {
		try {
			System.out.println("成功采集共 "+GetUrlContext.getAllNews("1").size()+" 条");
			String s= CacheManager.getCache("news_1").getValue().toString();
            System.out.println(s);
		} catch (Exception e) {
			log.info("执行异常!");
		}
	}
}
