package com.ymorning.common.utils;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class FlightDataTimerTaskListener implements ServletContextListener {
	public void contextInitialized(ServletContextEvent event) {
		new TimerManager();
	}
	public void contextDestroyed(ServletContextEvent event) {
	}
}
