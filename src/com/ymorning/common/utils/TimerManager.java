package com.ymorning.common.utils;

import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import com.ymorning.common.main.FlightDataTimerTask;

public class TimerManager {

	 //时间间隔
	 private static final long PERIOD_DAY =  5000;  //3 * 60 * 60 *
	 public TimerManager() {
	  Calendar calendar = Calendar.getInstance();
	  //每天凌晨7点执行，每隔3小时执行一次
	  calendar.set(Calendar.HOUR_OF_DAY,7);
	  calendar.set(Calendar.MINUTE, 0);
	  calendar.set(Calendar.SECOND, 0);
	  Date date=calendar.getTime(); //第一次执行定时任务的时间
	  System.out.println(date);
	  //当大于下午7点停止，当小于下午7点时每隔3小时执行一次
	  calendar.setTime(date);
      if(calendar.get(Calendar.HOUR_OF_DAY)>19){
    	  date = this.addDay(date, 1);
      }else{
    	  calendar.add(Calendar.HOUR_OF_DAY,3);
	      date = calendar.getTime();  
      }
	  Timer timer = new Timer();
	  FlightDataTimerTask task = new FlightDataTimerTask();
	  //指定的任务在指定的时间开始进行重复执行。
	  timer.schedule(task,date,PERIOD_DAY);
	 }
	 // 增加或减少天数
	 public Date addDay(Date date, int num) {
	  Calendar startDT = Calendar.getInstance();
	  startDT.setTime(date);
	  startDT.add(Calendar.DAY_OF_MONTH, num);
	  return startDT.getTime();
	 }
	}
